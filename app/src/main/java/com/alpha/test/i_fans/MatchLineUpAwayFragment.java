package com.alpha.test.i_fans;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import oogbox.api.odoo.OdooClient;
import oogbox.api.odoo.client.OdooVersion;
import oogbox.api.odoo.client.helper.data.OdooRecord;
import oogbox.api.odoo.client.helper.data.OdooResult;
import oogbox.api.odoo.client.helper.utils.ODomain;
import oogbox.api.odoo.client.helper.utils.OdooFields;
import oogbox.api.odoo.client.listeners.IOdooResponse;
import oogbox.api.odoo.client.listeners.OdooConnectListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class MatchLineUpAwayFragment extends Fragment {

    private View rootView;
    RecyclerView rvLineUpAwayCore,rvLineUpAway;
    SwipeRefreshLayout swiper;
    ArrayList<MatchLineUp> ArrayListMatchLineUpAwayCore;
    ArrayList<MatchLineUp> ArrayListMatchLineUpAway;
    OdooClient client;
    SharedPrefManager sharedPrefManager;
    AdapterLineUpHome adapterCore;
    AdapterLineUpHome adapter;

    public MatchLineUpAwayFragment() {
        // Required empty public constructor
    }

    public static MatchLineUpAwayFragment newInstance() {
        Bundle args = new Bundle();
        MatchLineUpAwayFragment fragment = new MatchLineUpAwayFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView==null){
            rootView = inflater.inflate(R.layout.fragment_match_line_up_away, container, false);
            rvLineUpAway = rootView.findViewById(R.id.rv_recycler_view_match_line_up_away);
            rvLineUpAwayCore = rootView.findViewById(R.id.rv_recycler_view_match_line_up_away_core);
            swiper = rootView.findViewById(R.id.swiperefresh_match_line_up_away);
            swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    new LineUpAwayTask().execute();
                }
            });
            sharedPrefManager = new  SharedPrefManager(getContext());
            adapter = new AdapterLineUpHome(ArrayListMatchLineUpAway);
            adapterCore = new AdapterLineUpHome(ArrayListMatchLineUpAwayCore);
            rvLineUpAway.setAdapter(adapter);
            rvLineUpAway.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvLineUpAwayCore.setAdapter(adapterCore);
            rvLineUpAwayCore.setLayoutManager(new LinearLayoutManager(getActivity()));
            new LineUpAwayTask().execute();

        }
        return rootView;
    }

    public class LineUpAwayTask extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            swiper.setRefreshing(true);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ArrayListMatchLineUpAway = new ArrayList<>();
            ArrayListMatchLineUpAwayCore = new ArrayList<>();
            client = new OdooClient.Builder(getContext())
                    .setHost(sharedPrefManager.getSP_Host_url())
                    .setSession(sharedPrefManager.getSpSessionId())
                    .setSynchronizedRequests(false)
                    .setConnectListener(new OdooConnectListener() {
                        @Override
                        public void onConnected(OdooVersion version) {
                            ODomain domain = new ODomain();
                            domain.add("jadwal_id", "=",getActivity().getIntent().getExtras().get("id_jadwal"));

                            OdooFields fields = new OdooFields();
                            fields.addAll("id","jadwal_id","home", "player_id","department_id","job_id","no_punggung","club_id","status_pemain");

                            int offset = 0;
                            int limit = 0;

                            String sorting = "id DESC";

                            client.searchRead("persebaya.line.up.away", domain, fields, offset, limit, sorting, new IOdooResponse() {
                                @Override
                                public void onResult(OdooResult result) {
                                    OdooRecord[] records = result.getRecords();
                                    for (OdooRecord record : records) {
                                        if (record.getString("status_pemain").equalsIgnoreCase("core")){
                                            ArrayListMatchLineUpAwayCore.add(new MatchLineUp(
                                                    String.valueOf(record.getInt("id")),
                                                    record.getString("jadwal_id"),
                                                    record.getString("player_id"),
                                                    String.valueOf(Math.round(record.getFloat("no_punggung"))),
                                                    record.getString("job_id"),
                                                    record.getString("club_id"),
                                                    record.getString("status_pemain")
                                            ));
                                        }else {
                                            ArrayListMatchLineUpAway.add(new MatchLineUp(
                                                    String.valueOf(record.getInt("id")),
                                                    record.getString("jadwal_id"),
                                                    record.getString("player_id"),
                                                    String.valueOf(Math.round(record.getFloat("no_punggung"))),
                                                    record.getString("job_id"),
                                                    record.getString("club_id"),
                                                    record.getString("status_pemain")
                                            ));
                                        }
                                    }
                                    adapter = new AdapterLineUpHome(ArrayListMatchLineUpAway);
                                    adapterCore = new AdapterLineUpHome(ArrayListMatchLineUpAwayCore);
                                    rvLineUpAway.setAdapter(adapter);
                                    rvLineUpAwayCore.setAdapter(adapterCore);
                                    adapter.notifyDataSetChanged();
                                    adapterCore.notifyDataSetChanged();
                                    swiper.setRefreshing(false);
                                }
                            });
                        }
                    }).build();
            return null;

        }
    }

}
